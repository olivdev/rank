from requests import get, post, put, delete
from must import *


def test_get_home():
    assert get(url_home).json()==home_dict


def test_get_dict():
    assert get(url_dict).json()==dict_dict


def test_get_rank():
    assert get(url_rank).json()==rank_dict


def test_post_up():
    assert post(url_up, data=up_dict).json()['data']==up_dict


def test_put_new():
    assert put(url_new, data=new_dict).json()['data']==new_dict


def test_delete_del():
    assert delete(url_del, data=del_dict).json()['data']==del_dict