# Banco de Dados


### *Esquemas de Tabelas:*

    Tabelas: NomeDoAluno
    Colunas: Data, Aula, Modulo, A, B, C, D, E, F, G, Score
    Descrição: 
        Data: Timestamp do momento que foi salvo
        Aula: Número da aula (0 até 48)
        Módulo: Número do modulo (1 até 3)
        A...G: Pontuação sobre comportamento
        Score: Total de Pontos

    Ex:
    | Data        | Aula | Modulo | A | B | C | D | E | F | G | Score |
    | timestamp01 |  16  |    1   | 1 | 1 | 0 | 1 | 1 | 1 | 0 |  1000 |
    | timestamp02 |   1  |    2   | 1 | 1 | 0 | 1 | 1 | 1 | 0 |  1600 |
    | timestamp03 |   2  |    2   | 1 | 1 | 1 | 1 | 1 | 1 | 0 |  2100 |
    | timestamp04 |   3  |    2   | 1 | 1 | 0 | 1 | 1 | 1 | 0 |  3200 |
    | timestamp05 |   4  |    2   | 1 | 1 | 0 | 1 | 1 | 1 | 0 |  3800 |
    | timestamp06 |   5  |    2   | 1 | 1 | 0 | 1 | 1 | 1 | 0 |  4300 |


# Rotas

### GET: */dict*

*Retorna um Dicionário com breve descrição de cada aula*

### GET: */rank*

*Mapeia todas as tabelase filtra por:*

    if TabelaName != "dict" && tabelaName != "del.."
        return Tabelas

*Filtra as tabelas resultantes por linha mais recente*

### POST: */up*

*Recebe parametros e cria uma nova linha na tabela filtrada por:*

    In Tabela == Name; add_new_row = Params

### PUT: */new*

*Adiciona uma nova tabela:*

    Create new Tabela = Name; add_new_row = Params

### DELETE: */del*

*Renomeia a tabela para "delAluno01":*

    count = 0
    Map Tabelas start_with "del" and count += 1
    count += 1
    Rename Tabela == Name for "delAluno{count}"



