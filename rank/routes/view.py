from flask import Flask, request
from flask_restful import Resource, Api

app = Flask(__name__)
api = Api(app)


class Dict(Resource):
    def get(self):
        return {"status": "dict"}


class Home(Resource):
    def get(self):
        return {"status": "home"}


class Rank(Resource):
    def get(self):
        rank = {}
        rank['status'] = 'rank'
        rank['data'] = ["Lucas", "#ffc", 2300]
    
        return rank


class Update_student(Resource):
    def post(self):
        upd = {}
        r = request.form
        upd['status'] = "up"
        upd['data'] = r

        return upd


class New_student(Resource):
    def put(self):
        r = request.form
        new = {}
        new['status'] = 'new'
        new['data'] = r
        
        return new

class Delete_student(Resource):
    def delete(self):
        r = request.form
        del_ = {}
        del_["status"] = "del"
        del_['data'] = r
        
        return del_


api.add_resource(Home, '/')
api.add_resource(Dict, '/dict')
api.add_resource(Rank, '/rank')
api.add_resource(Update_student, '/up')
api.add_resource(New_student, '/new')
api.add_resource(Delete_student, '/del')
